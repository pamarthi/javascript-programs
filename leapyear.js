function check_leapyear(n)
{
    let year=n;
    if((year%4==0) && (year % 100 !=0) || ( year % 400==0))
    {
        console.log(year+" is a leap year");  
    }
    else{
        console.log(year+" is not a leap year");  
    }
}
check_leapyear(2017);
check_leapyear(2016);

